library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity cu_fsm is
    Port ( sys_clk_100        : in STD_LOGIC;
           reset_n              : in STD_LOGIC;
    
           sw                   : in STD_LOGIC_VECTOR (7 downto 0);
           cw                   : out STD_LOGIC_VECTOR (4 downto 0));
end cu_fsm;

architecture Behavioral of cu_fsm is

--sw components
signal UIcodec_rdy, ui_clk, ui_clk_sync_rst, init_calib_complete, app_rdy : std_logic;
signal app_wdf_rdy, app_rd_data_valid : std_logic;

--cw components
signal app_en, app_cmd, app_wdf_wren, app_wdf_end, addr_inc : std_logic;

type state_type is (IDLEHI, IDLELO, CALIB, SETW, WRITE, SETR, READ, ADDRINC);
signal cState : state_type := IDLEHI;

begin

UIcodec_rdy         <= sw(0);
ui_clk              <= sw(1);
ui_clk_sync_rst     <= sw(2);
init_calib_complete <= sw(3);
app_rdy             <= sw(4);
app_wdf_rdy         <= sw(5);
app_rd_data_valid   <= sw(6);

cw(0) <= app_en;
cw(1) <= app_cmd;
cw(2) <= app_wdf_wren;
cw(3) <= app_wdf_end;
cw(4) <= addr_inc;

state_transition : process(ui_clk)
begin
if rising_edge(ui_clk) then
if ui_clk_sync_rst = '1' then
    cState <= IDLEHI;
else
    case cState is
        when IDLEHI =>
            if UIcodec_rdy = '0' then
                cState <= IDLELO; end if;
        when IDLELO =>
            if UIcodec_rdy = '1' then
                cState <= CALIB; end if;
        when CALIB =>
            if init_calib_complete = '1' then
                cState <= SETW; end if;
        when SETW =>
            if app_rdy = '1' then
                cState <= WRITE; end if;
        when WRITE =>
            if app_wdf_rdy = '1' then
                cState <= SETR; end if;
        when SETR =>
            if app_rdy = '1' then
                cState <= READ; end if;
        when READ =>
            if app_rd_data_valid = '1' then
                cState <= ADDRINC; end if;
        when others =>
            cState <= IDLEHI;
    end case;
end if;
end if;
end process;

en : process(cState)
begin
    if cState = SETW or cState = SETR then
        app_en <= '1';
    else app_en <= '0'; end if;
end process;

cmd : process(cState)
begin
    if cState = SETR then
        app_cmd <= '1';
    else app_cmd <= '0'; end if; --implies write command
end process;

wr : process(cState)
begin
    if cState = WRITE then
        app_wdf_wren <= '1';
        app_wdf_end <= '1';
    else 
        app_wdf_wren <= '0';
        app_wdf_end <= '0'; end if;
end process;

inc : process(cState)
begin
    if cState = ADDRINC then
        addr_inc <= '1';
    else addr_inc <= '0'; end if;
end process;

end Behavioral;