library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package component_library is

component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;
  clk_out2          : out    std_logic;
  -- Status and control signals
  resetn             : in     std_logic;
  locked            : out    std_logic;
  clk_in1           : in     std_logic
 );
end component;

component Audio_Codec_Wrapper is
    Port ( clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        ac_mclk : out STD_LOGIC;
        ac_adc_sdata : in STD_LOGIC;
        ac_dac_sdata : out STD_LOGIC;
        ac_bclk : out STD_LOGIC;
        ac_lrclk : out STD_LOGIC;
        ready : out STD_LOGIC;
        L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
        R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
        L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
        R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
        scl : inout STD_LOGIC;
        sda : inout STD_LOGIC);
end component;

component mig_7
  port (
      ddr3_dq       : inout std_logic_vector(15 downto 0);
      ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
      ddr3_dqs_n    : inout std_logic_vector(1 downto 0);

      ddr3_addr     : out   std_logic_vector(14 downto 0);
      ddr3_ba       : out   std_logic_vector(2 downto 0);
      ddr3_ras_n    : out   std_logic;
      ddr3_cas_n    : out   std_logic;
      ddr3_we_n     : out   std_logic;
      ddr3_reset_n  : out   std_logic;
      ddr3_ck_p     : out   std_logic_vector(0 downto 0);
      ddr3_ck_n     : out   std_logic_vector(0 downto 0);
      ddr3_cke      : out   std_logic_vector(0 downto 0);
      ddr3_dm       : out   std_logic_vector(1 downto 0);
      ddr3_odt      : out   std_logic_vector(0 downto 0);
      app_addr                  : in    std_logic_vector(28 downto 0);
      app_cmd                   : in    std_logic_vector(2 downto 0);
      app_en                    : in    std_logic;
      app_wdf_data              : in    std_logic_vector(127 downto 0);
      app_wdf_end               : in    std_logic;
      app_wdf_mask         : in    std_logic_vector(15 downto 0);
      app_wdf_wren              : in    std_logic;
      app_rd_data               : out   std_logic_vector(127 downto 0);
      app_rd_data_end           : out   std_logic;
      app_rd_data_valid         : out   std_logic;
      app_rdy                   : out   std_logic;
      app_wdf_rdy               : out   std_logic;
      app_sr_req                : in    std_logic;
      app_ref_req               : in    std_logic;
      app_zq_req                : in    std_logic;
      app_sr_active             : out   std_logic;
      app_ref_ack               : out   std_logic;
      app_zq_ack                : out   std_logic;
      ui_clk                    : out   std_logic;
      ui_clk_sync_rst           : out   std_logic;
      init_calib_complete       : out   std_logic;
      -- System Clock Ports
      sys_clk_i                      : in    std_logic;
      -- Reference Clock Ports
    sys_rst                     : in    std_logic
  );
end component mig_7;

component dp is
    Port ( sys_clk_100      : in STD_LOGIC;
           sys_clk_200      : in STD_LOGIC;
           reset_n  : in STD_LOGIC;
           switch   : in std_logic_vector(7 downto 0);
           led      : out std_logic_vector(7 downto 0);
           
           cw       : in STD_LOGIC_VECTOR (4 downto 0);
           sw       : out STD_LOGIC_VECTOR (7 downto 0);
           
           --DDR3
           ddr3_dq       : inout std_logic_vector(15 downto 0);
           ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
           ddr3_dqs_n    : inout std_logic_vector(1 downto 0);
           ddr3_addr     : out   std_logic_vector(14 downto 0);
           ddr3_ba       : out   std_logic_vector(2 downto 0);
           ddr3_ras_n    : out   std_logic;
           ddr3_cas_n    : out   std_logic;
           ddr3_we_n     : out   std_logic;
           ddr3_reset_n  : out   std_logic;
           ddr3_ck_p     : out   std_logic_vector(0 downto 0);
           ddr3_ck_n     : out   std_logic_vector(0 downto 0);
           ddr3_cke      : out   std_logic_vector(0 downto 0);
           ddr3_odt      : out   std_logic_vector(0 downto 0);
           ddr3_dm       : out   std_logic_vector(1 downto 0);
           
           --Audio Codec/I2C
           ac_mclk      : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk      : out STD_LOGIC;
           ac_lrclk     : out STD_LOGIC;
           scl          : inout STD_LOGIC;
           sda          : inout STD_LOGIC;
           
           --Video
           tmds         : out std_logic_vector(3 downto 0);
           tmdsb        : out std_logic_vector(3 downto 0));
end component;

component mixer is
    Port ( clk          : in STD_LOGIC;
       reset        : in STD_LOGIC;
       pb_switch    : in STD_LOGIC_VECTOR (3 downto 0);
       
       mem_word     : in STD_LOGIC_VECTOR (127 downto 0);
       L_bus_out    : in STD_LOGIC_VECTOR (17 downto 0);
       R_bus_out    : in STD_LOGIC_VECTOR (17 downto 0);
       
       L_bus_in : out STD_LOGIC_VECTOR (17 downto 0);
       R_bus_in : out STD_LOGIC_VECTOR (17 downto 0));
end component;

component cu_fsm is
    Port ( sys_clk_100        : in STD_LOGIC;
           reset_n              : in STD_LOGIC;
    
           sw                   : in STD_LOGIC_VECTOR (7 downto 0);
           cw                   : out STD_LOGIC_VECTOR (4 downto 0));
end component;

component oscope_dp is
    Port (  clk : in  STD_LOGIC;
        reset_n : in  STD_LOGIC;
        tmds : out std_logic_vector(3 downto 0);
        tmdsb : out std_logic_vector(3 downto 0);
        ch1, ch2, ch3, ch4 : in std_logic_vector(17 downto 0);
        sw: out std_logic_vector(1 downto 0);
        cw: in std_logic_vector (2 downto 0));
end component;

component oscope_fsm is
    Port ( clk     : in  STD_LOGIC;
       reset_n : in  STD_LOGIC;
       rdy     : in  std_logic;
       sw      : in std_logic_vector(1 downto 0);
       cw      : out std_logic_vector (2 downto 0) );
end component;

component transform is
    Port ( sign : in STD_LOGIC_VECTOR (17 downto 0);
           un_sign : out STD_LOGIC_VECTOR (9 downto 0));
end component;

component video is
    Port ( clk : in  STD_LOGIC;
       reset_n : in  STD_LOGIC;
       tmds : out  STD_LOGIC_VECTOR (3 downto 0);
       tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
       trigger_time: in unsigned(9 downto 0);
       trigger_volt: in unsigned (9 downto 0);
       row: out unsigned(9 downto 0);
       column: out unsigned(9 downto 0);
       ch1: in std_logic;
       ch2: in std_logic;
       ch3: in std_logic;
       ch4: in std_logic);
end component;

end component_library;