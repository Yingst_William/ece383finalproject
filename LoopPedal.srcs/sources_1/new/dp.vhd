library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.component_library.all;
use IEEE.NUMERIC_STD.ALL;

entity dp is
    Port ( sys_clk_100      : in STD_LOGIC;
           sys_clk_200      : in STD_LOGIC;
           reset_n          : in STD_LOGIC;
           switch           : in std_logic_vector(7 downto 0);
           led              : out std_logic_vector(7 downto 0);
           
           cw               : in STD_LOGIC_VECTOR (4 downto 0);
           sw               : out STD_LOGIC_VECTOR (7 downto 0);
           
           --DDR3
           ddr3_dq       : inout std_logic_vector(15 downto 0);
           ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
           ddr3_dqs_n    : inout std_logic_vector(1 downto 0);
           ddr3_addr     : out   std_logic_vector(14 downto 0);
           ddr3_ba       : out   std_logic_vector(2 downto 0);
           ddr3_ras_n    : out   std_logic;
           ddr3_cas_n    : out   std_logic;
           ddr3_we_n     : out   std_logic;
           ddr3_reset_n  : out   std_logic;
           ddr3_ck_p     : out   std_logic_vector(0 downto 0);
           ddr3_ck_n     : out   std_logic_vector(0 downto 0);
           ddr3_cke      : out   std_logic_vector(0 downto 0);
           ddr3_odt      : out   std_logic_vector(0 downto 0);
           ddr3_dm       : out   std_logic_vector(1 downto 0);
           
           --Audio Codec/I2C
           ac_mclk      : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk      : out STD_LOGIC;
           ac_lrclk     : out STD_LOGIC;
           scl          : inout STD_LOGIC;
           sda          : inout STD_LOGIC;
           
           --Video
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0)
           
           );
end dp;

architecture Behavioral of dp is

--audio signals
signal L_bus_in, R_bus_in, L_bus_out, R_bus_out : STD_LOGIC_VECTOR(17 downto 0);
signal UIL_bus_out, UIR_bus_out : std_logic_vector(17 downto 0);
signal codec_rdy, UIcodec_rdy : std_logic;
signal dcReg : std_logic_vector(1 downto 0);

--memory signals
signal init_calib_complete, ui_clk_sync_rst, ui_clk, app_rdy, app_en : std_logic;
signal app_wdf_rdy, app_rd_data_valid : std_logic;
signal app_cmd : std_logic_vector(2 downto 0) := "000";
signal app_addr : STD_LOGIC_VECTOR(28 downto 0) := (others => '0');
signal app_wdf_data, app_rd_data, mem_word : STD_LOGIC_VECTOR(127 downto 0);
signal app_wdf_wren, app_wdf_end, addr_inc : STD_LOGIC;
signal app_wdf_mask : std_logic_vector(15 downto 0);

--track memory
signal first_rec : std_logic := '1';
signal addr_wrap : std_logic_vector(28 downto 0) := x"FFFFFF" & "11000";

--input
signal sys_rst, rff1, sw_cntr_rst : std_logic := '0';
signal switchPipe : std_logic_vector(15 downto 0) := (others => '0');
signal UI_switch, lastUI_switch : std_logic_vector(7 downto 0) := (others => '0');
signal sw_cntr : unsigned(20 downto 0) := (others => '0');

--o-scope
signal ch1, ch2, ch3, ch4 : std_logic_vector(17 downto 0);
signal os_sw : std_logic_vector(1 downto 0);
signal os_cw : std_logic_vector(2 downto 0);

begin

wrapper : Audio_Codec_Wrapper
port map (
    clk => sys_clk_100,
    reset_n => sys_rst,
    ac_mclk => ac_mclk,
    ac_adc_sdata => ac_adc_sdata,
    ac_dac_sdata => ac_dac_sdata,
    ac_bclk => ac_bclk,
    ac_lrclk => ac_lrclk,
    ready => codec_rdy,
    L_bus_in => L_bus_in,
    R_bus_in => R_bus_in,
    L_bus_out => L_bus_out,
    R_bus_out => R_bus_out,
    scl => scl,
    sda => sda
);
    
u_mig_7 : mig_7
port map (
    -- Memory interface ports
    ddr3_addr                      => ddr3_addr,
    ddr3_ba                        => ddr3_ba,
    ddr3_cas_n                     => ddr3_cas_n,
    ddr3_ck_n                      => ddr3_ck_n,
    ddr3_ck_p                      => ddr3_ck_p,
    ddr3_cke                       => ddr3_cke,
    ddr3_ras_n                     => ddr3_ras_n,
    ddr3_reset_n                   => ddr3_reset_n,
    ddr3_we_n                      => ddr3_we_n,
    ddr3_dq                        => ddr3_dq,
    ddr3_dqs_n                     => ddr3_dqs_n,
    ddr3_dqs_p                     => ddr3_dqs_p,
    ddr3_odt                       => ddr3_odt,
    ddr3_dm                        => ddr3_dm,
    -- Application interface ports
    init_calib_complete            => init_calib_complete,
    app_addr                       => app_addr,
    app_cmd                        => app_cmd,
    app_en                         => app_en,
    app_wdf_data                   => app_wdf_data,
    app_wdf_end                    => app_wdf_end,
    app_wdf_mask                   => app_wdf_mask,
    app_wdf_wren                   => app_wdf_wren,
    app_rd_data                    => app_rd_data,
    app_rd_data_end                => open,
    app_rd_data_valid              => app_rd_data_valid,
    app_rdy                        => app_rdy,
    app_wdf_rdy                    => app_wdf_rdy,
    app_sr_req                     => '0',
    app_ref_req                    => '0',
    app_zq_req                     => '0',
    app_sr_active                  => open,
    app_ref_ack                    => open,
    app_zq_ack                     => open,
    ui_clk                         => ui_clk,
    ui_clk_sync_rst                => ui_clk_sync_rst,
    -- System Clock Ports
    sys_clk_i                      => sys_clk_200,
    -- Reference Clock Ports
    sys_rst                        => sys_rst
);

mixer_inst : mixer
port map (
    clk => ui_clk,
    reset => ui_clk_sync_rst,
    pb_switch => UI_switch(3 downto 0),
    mem_word => mem_word,
    L_bus_out => L_bus_out,
    R_bus_out => R_bus_out,
    L_bus_in => L_bus_in,
    R_bus_in => R_bus_in);
    
scope : oscope_dp
port map (
    clk => ui_clk,
    reset_n => sys_rst,
    tmds => tmds,
    tmdsb => tmdsb,
    ch1 => ch1,
    ch2 => ch2,
    ch3 => ch3,
    ch4 => ch4,
    sw => os_sw,
    cw => os_cw);
    
scope_fsm : oscope_fsm
port map(
    clk => ui_clk,
    reset_n => sys_rst,
    rdy => UIcodec_rdy,
    sw => os_sw,
    cw => os_cw);
    
sw(0) <= UIcodec_rdy;
sw(1) <= ui_clk;                --must be used for MIG-related logic
sw(2) <= ui_clk_sync_rst;       --same
sw(3) <= init_calib_complete;
sw(4) <= app_rdy;
sw(5) <= app_wdf_rdy;
sw(6) <= app_rd_data_valid;
sw(7) <= '0';

app_en          <= cw(0);
app_cmd         <= "00" & cw(1);
app_wdf_wren    <= cw(2);
app_wdf_end     <= cw(3);
addr_inc        <= cw(4);  

-- convert board reset into sys_clk_200 domain
process(sys_clk_200, reset_n)
begin
    if reset_n = '0' then
        rff1 <= '0';
        sys_rst <= '0';
    elsif (sys_clk_200'event and sys_clk_200 = '1') then
        rff1 <= '1';
        sys_rst <= rff1;
    end if;
end process;

--converting codec_rdy to UI_clk domain
process(ui_clk)
begin
    if rising_edge(ui_clk) then
        dcReg <= dcReg(0) & codec_rdy;
        UIcodec_rdy <= dcReg(1); 
    end if;
end process;

--converting switches into UI_clk domain
--debounce timer restart signal
sw_cntr_rst <= '1' when (switchPipe(15 downto 8) /= switchPipe(7 downto 0)) else
               '0';
process(ui_clk)
begin
    if rising_edge(ui_clk) then
        --route switch values through two flipflops for stability
        switchPipe <= switchPipe(7 downto 0) & switch;
        if sw_cntr_rst = '1' then
            sw_cntr <= (others => '0');
        elsif sw_cntr < 2000000 then --20 ms
            sw_cntr <= sw_cntr + 1;
        else
            lastUI_switch <= UI_switch;
            UI_switch <= switchPipe(15 downto 8); end if;
    end if;
end process;

--loading codec output data on deck to write
process(UIcodec_rdy)
begin
    if rising_edge(UIcodec_rdy) then
        if first_rec = '0' then
            app_wdf_data(127 downto 112)    <= L_bus_out(17 downto 2);
            app_wdf_data(111 downto 96)     <= R_bus_out(17 downto 2);
            app_wdf_data(95 downto 80)      <= L_bus_out(17 downto 2);
            app_wdf_data(79 downto 64)      <= R_bus_out(17 downto 2);
            app_wdf_data(63 downto 48)      <= L_bus_out(17 downto 2);
            app_wdf_data(47 downto 32)      <= R_bus_out(17 downto 2);
            app_wdf_data(31 downto 16)      <= L_bus_out(17 downto 2);
            app_wdf_data(15 downto 0)       <= R_bus_out(17 downto 2); 
        else
            case UI_switch(7 downto 4) is
            when "1000" =>
                app_wdf_data(127 downto 112)    <= L_bus_out(17 downto 2);
                app_wdf_data(111 downto 96)     <= R_bus_out(17 downto 2);
                app_wdf_data(95 downto 80)      <= x"0000";
                app_wdf_data(79 downto 64)      <= x"0000";
                app_wdf_data(63 downto 48)      <= x"0000";
                app_wdf_data(47 downto 32)      <= x"0000";
                app_wdf_data(31 downto 16)      <= x"0000";
                app_wdf_data(15 downto 0)       <= x"0000";
            when "0100" =>
                app_wdf_data(127 downto 112)    <= x"0000";
                app_wdf_data(111 downto 96)     <= x"0000";
                app_wdf_data(95 downto 80)      <= L_bus_out(17 downto 2);
                app_wdf_data(79 downto 64)      <= R_bus_out(17 downto 2);
                app_wdf_data(63 downto 48)      <= x"0000";
                app_wdf_data(47 downto 32)      <= x"0000";
                app_wdf_data(31 downto 16)      <= x"0000";
                app_wdf_data(15 downto 0)       <= x"0000";                            
            when "0010" =>
                app_wdf_data(127 downto 112)    <= x"0000";
                app_wdf_data(111 downto 96)     <= x"0000";
                app_wdf_data(95 downto 80)      <= x"0000";
                app_wdf_data(79 downto 64)      <= x"0000";
                app_wdf_data(63 downto 48)      <= L_bus_out(17 downto 2);
                app_wdf_data(47 downto 32)      <= R_bus_out(17 downto 2);
                app_wdf_data(31 downto 16)      <= x"0000";
                app_wdf_data(15 downto 0)       <= x"0000";
            when "0001" =>
                app_wdf_data(127 downto 112)    <= x"0000";
                app_wdf_data(111 downto 96)     <= x"0000";
                app_wdf_data(95 downto 80)      <= x"0000";
                app_wdf_data(79 downto 64)      <= x"0000";
                app_wdf_data(63 downto 48)      <= x"0000";
                app_wdf_data(47 downto 32)      <= x"0000";
                app_wdf_data(31 downto 16)      <= L_bus_out(17 downto 2);
                app_wdf_data(15 downto 0)       <= R_bus_out(17 downto 2);
            when others =>
                app_wdf_data <= (others => '0');
            end case;
        end if;
    end if;
end process;

--reading from DDR3 when valid
process(ui_clk)
begin
    if rising_edge(ui_clk) then
        if app_rd_data_valid = '1' then
            mem_word <= app_rd_data;
        end if;
    end if;
end process;

--sending L channels into o-scope
ch1 <= mem_word(31 downto 16) & "00" when UI_switch(0) = '1' else
       (others => '0');
ch2 <= mem_word(63 downto 48) & "00" when UI_switch(1) = '1' else
       (others => '0');
ch3 <= mem_word(95 downto 80) & "00" when UI_switch(2) = '1' else
       (others => '0');
ch4 <= mem_word(127 downto 112) & "00" when UI_switch(3) = '1' else
       (others => '0');               

--first track record/track length set
process(ui_clk)
begin
    if rising_edge(ui_clk) then
        if ui_clk_sync_rst = '1' then
            addr_wrap <= x"FFFFFF" & "11000";
            first_rec <= '1';
        elsif (first_rec = '1') and (lastUI_switch(7 downto 4) > x"0") and (UI_switch(7 downto 4) = x"0") then
            addr_wrap <= app_addr; 
            first_rec <= '0';
        end if;
    end if;
end process;

--incrementing address by 8 (word size/actual word size)
--                          (      128/16              )
process(ui_clk)
begin
    if rising_edge(ui_clk) then
        if ui_clk_sync_rst = '1' then
            app_addr <= (others => '0');
        elsif addr_inc = '1' then
            if app_addr >= addr_wrap then
                app_addr <= (others => '0');
            else 
                if first_rec = '0' then
                    app_addr <= std_logic_vector(unsigned(app_addr) + 8); 
                elsif UI_switch(7 downto 4) = "0001" or
                      UI_switch(7 downto 4) = "0010" or
                      UI_switch(7 downto 4) = "0100" or
                      UI_switch(7 downto 4) = "1000" then
                      app_addr <= std_logic_vector(unsigned(app_addr) + 8);
                end if;
            end if;
        end if;
    end if;
end process;

--track record
--one-hot
app_wdf_mask <= x"FFF0" when UI_switch(7 downto 4) = "0001" and first_rec = '0' else
                x"FF0F" when UI_switch(7 downto 4) = "0010" and first_rec = '0' else
                x"F0FF" when UI_switch(7 downto 4) = "0100" and first_rec = '0' else
                x"0FFF" when UI_switch(7 downto 4) = "1000" and first_rec = '0' else
                x"0000" when first_rec = '1' else
                x"FFFF";

--LED connection for watching address space
led <= app_addr(26 downto 19);
end Behavioral;