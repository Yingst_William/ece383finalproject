library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mixer is
    Port ( clk          : in STD_LOGIC;
           reset        : in STD_LOGIC;
           pb_switch    : in STD_LOGIC_VECTOR (3 downto 0);
           
           mem_word     : in STD_LOGIC_VECTOR (127 downto 0);
           L_bus_out    : in STD_LOGIC_VECTOR (17 downto 0);
           R_bus_out    : in STD_LOGIC_VECTOR (17 downto 0);
           
           L_bus_in : out STD_LOGIC_VECTOR (17 downto 0);
           R_bus_in : out STD_LOGIC_VECTOR (17 downto 0));
end mixer;

architecture Behavioral of mixer is

signal t1_L, t2_L, t3_L, t4_L, live_L : signed(17 downto 0);
signal t1_R, t2_R, t3_R, t4_R, live_R : signed(17 downto 0);

signal total_sum_L, total_sum_R : integer;

signal out_L, out_R : STD_LOGIC_VECTOR (17 downto 0);

begin

t1_L <= signed(mem_word(31 downto 16) & "00") when pb_switch(0) = '1' else
        x"0000" & "00";
t1_R <= signed(mem_word(15 downto 0) & "00")  when pb_switch(0) = '1' else
        x"0000" & "00";

t2_L <= signed(mem_word(63 downto 48) & "00") when pb_switch(1) = '1' else
        x"0000" & "00";
t2_R <= signed(mem_word(47 downto 32) & "00") when pb_switch(1) = '1' else
        x"0000" & "00";
        
t3_L <= signed(mem_word(95 downto 80) & "00") when pb_switch(2) = '1' else
        x"0000" & "00";
t3_R <= signed(mem_word(79 downto 64) & "00") when pb_switch(2) = '1' else
        x"0000" & "00";
        
t4_L <= signed(mem_word(127 downto 112) & "00") when pb_switch(3) = '1' else
        x"0000" & "00";
t4_R <= signed(mem_word(111 downto 96) & "00")  when pb_switch(3) = '1' else
        x"0000" & "00";
        
live_L <= signed(L_bus_out);
live_R <= signed(R_bus_out);

total_sum_L <= to_integer(t1_L) + to_integer(t2_L) + to_integer(t3_L) + to_integer(t4_L) + to_integer(live_L);
total_sum_R <= to_integer(t1_R) + to_integer(t2_R) + to_integer(t3_R) + to_integer(t4_R) + to_integer(live_R);

--clipping protection
out_L <= ("01" & x"FFFF") when (total_sum_L >  131071) else
         ("10" & x"0000") when (total_sum_L < -131072) else
         std_logic_vector(to_signed(total_sum_L, 18));
         
out_R <= ("01" & x"FFFF") when (total_sum_R >  131071) else
         ("10" & x"0000") when (total_sum_R < -131072) else
         std_logic_vector(to_signed(total_sum_R, 18));

process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            L_bus_in <= (others => '0');
            R_bus_in <= (others => '0');
        else
            L_bus_in <= out_L;
            R_bus_in <= out_R;
        end if;
    end if;
end process;

end Behavioral;
