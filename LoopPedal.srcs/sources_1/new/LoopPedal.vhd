library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.component_library.all;
library UNISIM;
use UNISIM.vcomponents.all;

entity LoopPedal is
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           switch : in std_logic_vector(7 downto 0);
           led : out std_logic_vector(7 downto 0);
           
           --DDR3
           ddr3_dq       : inout std_logic_vector(15 downto 0);
           ddr3_dqs_p    : inout std_logic_vector(1 downto 0);
           ddr3_dqs_n    : inout std_logic_vector(1 downto 0);
           ddr3_addr     : out   std_logic_vector(14 downto 0);
           ddr3_ba       : out   std_logic_vector(2 downto 0);
           ddr3_ras_n    : out   std_logic;
           ddr3_cas_n    : out   std_logic;
           ddr3_we_n     : out   std_logic;
           ddr3_reset_n  : out   std_logic;
           ddr3_ck_p     : out   std_logic_vector(0 downto 0);
           ddr3_ck_n     : out   std_logic_vector(0 downto 0);
           ddr3_cke      : out   std_logic_vector(0 downto 0);
           ddr3_odt      : out   std_logic_vector(0 downto 0);
           ddr3_dm       : out   std_logic_vector(1 downto 0);
           
           --Audio Codec/I2C
           ac_mclk : out STD_LOGIC;
           ac_adc_sdata : in STD_LOGIC;
           ac_dac_sdata : out STD_LOGIC;
           ac_bclk : out STD_LOGIC;
           ac_lrclk : out STD_LOGIC;
           scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
           
           --HDMI Out
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0)
           );
end LoopPedal;

architecture Behavioral of LoopPedal is

signal sys_clk_100, sys_clk_200 : std_logic;

signal cw : std_logic_vector(4 downto 0);
signal sw : std_logic_vector(7 downto 0);

begin
        
    clk1 : clk_wiz_0
        port map ( 
        -- Clock out ports  
        clk_out1 => sys_clk_100,
        clk_out2 => sys_clk_200,
        -- Status and control signals
        resetn => reset_n,
        -- Clock in ports
        locked => open,
        clk_in1 => clk);
                
    dp_inst : dp
        port map (
        sys_clk_100 => sys_clk_100,
        sys_clk_200 => sys_clk_200,
        reset_n => reset_n,
        switch => switch,
        led => led,
        
        cw => cw,
        sw => sw,
               
        --DDR3
        ddr3_dq       => ddr3_dq,
        ddr3_dqs_p    => ddr3_dqs_p, 
        ddr3_dqs_n    => ddr3_dqs_n, 
        ddr3_addr     => ddr3_addr,
        ddr3_ba       => ddr3_ba, 
        ddr3_ras_n    => ddr3_ras_n,
        ddr3_cas_n    => ddr3_cas_n,
        ddr3_we_n     => ddr3_we_n, 
        ddr3_reset_n  => ddr3_reset_n,
        ddr3_ck_p     => ddr3_ck_p, 
        ddr3_ck_n     => ddr3_ck_n,
        ddr3_cke      => ddr3_cke,
        ddr3_odt      => ddr3_odt, 
        ddr3_dm       => ddr3_dm,
        
        --Audio Codec/I2C
        ac_mclk      => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk      => ac_bclk,
        ac_lrclk     => ac_lrclk,
        scl          => scl,
        sda          => sda,
        
        --Video
        tmds         => tmds,
        tmdsb        => tmdsb);
        
    cu_inst : cu_fsm
    port map (
        sys_clk_100  => sys_clk_100,
        reset_n => reset_n,
    
        sw      => sw,
        cw      => cw);
        
end Behavioral;
