library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.component_library.all;

entity oscope_dp is
    Port (  clk : in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            tmds : out std_logic_vector(3 downto 0);
            tmdsb : out std_logic_vector(3 downto 0);
            ch1, ch2, ch3, ch4 : in std_logic_vector(17 downto 0);
            sw: out std_logic_vector(1 downto 0);
            cw: in std_logic_vector (2 downto 0));
end oscope_dp;

    --------- sw table ----------
    -- (1) address max comparator
    -- (0) trigger AND
    
    -- WREN <= cw(2)    
    -- ctr <= cw(1 downto 0)
    -- ctr table ----
    -- reset | "00" | 
    --  hold | "01" |
    --    up | "10" |
    --  load | "11" |
    -----------------

architecture Behavioral of oscope_dp is
    
    -- trigger signals
    signal trigger_time : unsigned(9 downto 0) := to_unsigned(320, 10);
    signal trigger_volt : unsigned(9 downto 0) := to_unsigned(220, 10);
    signal trig_reg, avg : unsigned(9 downto 0);
    
    -- video-vhd in/out
    signal ch_1, ch_2, ch_3, ch_4 : std_logic;
    signal s_row, s_column : unsigned (9 downto 0);
    
    -- BRAM signals
    signal read1, read2, read3, read4 : std_logic_vector (9 downto 0);
    signal transOut1, transOut2, transOut3, transOut4 : std_logic_vector (9 downto 0);
    signal addr_s : std_logic_vector (9 downto 0);
    signal dIn1, dIn2, dIn3, dIn4 : std_logic_vector (9 downto 0);
    signal bram_reset : std_logic;
    signal WREN_s : std_logic;
    
    -- misc
    signal triggered : std_logic := '0';
    signal addrCnt : std_logic_vector (9 downto 0);
    
begin
    
    -- FSM CSAs
    sw(1) <= '1' when addrCnt = x"26C" else '0';
    sw(0) <= triggered;
    
     
    address_counter_proc : process(clk)
    begin
        if (rising_edge(clk)) then 
            if reset_n = '0' then 
                addrCnt <= "0000000000";
            else
                case cw(1 downto 0) is 
                    when "00" =>
                        addrCnt <= "0000000000";
                    when "01" =>
                        addrCnt <= addrCnt;
                    when "10" =>
                        addrCnt <= (addrCnt + "0000000001");
                    when others =>
                        addrCnt <= "0000010100";
                end case;
            end if;
        end if;
    end process;
    
    
    bram_reset <= not reset_n;
    dIn1 <= transOut1;
    dIn2 <= transOut2;
    dIn3 <= transOut3;
    dIn4 <= transOut4;
    addr_s <= addrCnt;
    WREN_s <= cw(2);
    
    
    BRAM_inst_1 : BRAM_SDP_MACRO 
    generic map(
        BRAM_SIZE => "18Kb", 			-- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
        DO_REG => 0,                    -- Optional output register disabled
        INIT => X"000000000000000000",  -- Initial values on output port
        INIT_FILE => "NONE",            -- Not sure how to initialize the RAM from a file
        WRITE_WIDTH => 10,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 10,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
        SRVAL => X"000000000000000000") -- Set/Reset value for port output
    port map(
        DO => read1,
        DI => dIn1,
        WRADDR => addr_s,
        RDADDR => std_logic_vector(s_column),
        WE => "11",
        rst => bram_reset,
        WREN => WREN_s,
        RDEN => '1',
        REGCE => '1',
        WRCLK => clk,
        RDCLK => clk);
    
    BRAM_inst_2 : BRAM_SDP_MACRO
    generic map(
        BRAM_SIZE => "18Kb",             -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
        DO_REG => 0,                    -- Optional output register disabled
        INIT => X"000000000000000000",  -- Initial values on output port
        INIT_FILE => "NONE",            -- Not sure how to initialize the RAM from a file
        WRITE_WIDTH => 10,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 10,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
        SRVAL => X"000000000000000000") -- Set/Reset value for port output
    port map(
        DO => read2,
        DI => dIn2,
        WRADDR => addr_s,
        RDADDR => std_logic_vector(s_column),
        WE => "11",
        rst => bram_reset,
        WREN => WREN_s,
        RDEN => '1',
        REGCE => '1',
        WRCLK => clk,
        RDCLK => clk);        
    
    BRAM_inst_3 : BRAM_SDP_MACRO
    generic map(
        BRAM_SIZE => "18Kb",             -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
        DO_REG => 0,                    -- Optional output register disabled
        INIT => X"000000000000000000",  -- Initial values on output port
        INIT_FILE => "NONE",            -- Not sure how to initialize the RAM from a file
        WRITE_WIDTH => 10,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 10,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
        SRVAL => X"000000000000000000") -- Set/Reset value for port output
    port map(
        DO => read3,
        DI => dIn3,
        WRADDR => addr_s,
        RDADDR => std_logic_vector(s_column),
        WE => "11",
        rst => bram_reset,
        WREN => WREN_s,
        RDEN => '1',
        REGCE => '1',
        WRCLK => clk,
        RDCLK => clk);      
        
    BRAM_inst_4 : BRAM_SDP_MACRO
    generic map(
        BRAM_SIZE => "18Kb",             -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
        DO_REG => 0,                    -- Optional output register disabled
        INIT => X"000000000000000000",  -- Initial values on output port
        INIT_FILE => "NONE",            -- Not sure how to initialize the RAM from a file
        WRITE_WIDTH => 10,              -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 10,               -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",  -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
        SRVAL => X"000000000000000000") -- Set/Reset value for port output
    port map(
        DO => read4,
        DI => dIn4,
        WRADDR => addr_s,
        RDADDR => std_logic_vector(s_column),
        WE => "11",
        rst => bram_reset,
        WREN => WREN_s,
        RDEN => '1',
        REGCE => '1',
        WRCLK => clk,
        RDCLK => clk);
            
    un_sign_inst_1 : transform port map(
        sign => ch1,
        un_sign => transOut1);
    un_sign_inst_2 : transform port map(
        sign => ch2,
        un_sign => transOut2);             
    un_sign_inst_3 : transform port map(
        sign => ch3,
        un_sign => transOut3);
    un_sign_inst_4 : transform port map(
        sign => ch4,
        un_sign => transOut4);
    
    avg <= to_unsigned(to_integer(unsigned(transOut1) + unsigned(transOut2) + unsigned(transOut3) + unsigned(transOut4)) / 4, 10);
    
    -- triggers on any channel more or less
    trigger : process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset_n = '0') then 
                triggered <= '0';
            elsif ((avg > trigger_volt) and (trig_reg < trigger_volt)) then 
                triggered <= '1';
            else
                triggered <= '0';
            end if;
            trig_reg <= avg;
        end if;
    end process;
    
        
    video_inst : video port map(
        clk => clk,
        reset_n => reset_n,
        tmds => tmds,
        tmdsb => tmdsb,
        trigger_time => trigger_time,
        trigger_volt => trigger_volt,
        row => s_row,
        column => s_column,
        ch1 => ch_1,
        ch2 => ch_2,
        ch3 => ch_3,
        ch4 => ch_4);
        
    ch_1 <= '1' when (std_logic_vector(s_row) = read1) else
            '0';
    ch_2 <= '1' when (std_logic_vector(s_row) = read2) else
            '0';
    ch_3 <= '1' when (std_logic_vector(s_row) = read3) else
            '0';
    ch_4 <= '1' when (std_logic_vector(s_row) = read4) else
            '0';
    
end Behavioral;