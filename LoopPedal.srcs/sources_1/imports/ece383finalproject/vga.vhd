--------------------------------------------------------------------
-- Name:	C2C Will Yingst
-- Date:	Jan 29, 2020
-- File:	vga.vhd
-- Assn:	Lab 1
-- Crs:	    ECE 383
--
-- Purp:	This file generates a standard VGA signal
--
-- Documentation:	No Help Received
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vga is
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch2: in std_logic;
			ch3: in std_logic;
			ch4: in std_logic);
end vga;

architecture behavioral of vga is

    signal v_clk : STD_LOGIC := '0';
    signal column_out, row_out : unsigned (9 downto 0) := "0000000000";
    
    component gen_mod_ctr is
        Generic ( max : integer);
        Port ( clk : in STD_LOGIC;
               reset : in STD_LOGIC;
               cnt : out unsigned (9 downto 0);
               roll : out STD_LOGIC);
    end component;
    
    component scopeFace is 
        Port( row : in  unsigned(9 downto 0);
              column : in  unsigned(9 downto 0);
              trigger_volt: in unsigned (9 downto 0);
              trigger_time: in unsigned (9 downto 0);
              ch1 : in std_logic;
              ch2 : in std_logic;
              ch3 : in std_logic;
              ch4 : in std_logic;
              r : out  std_logic_vector(7 downto 0);
              g : out  std_logic_vector(7 downto 0);
              b : out  std_logic_vector(7 downto 0));
    end component;
        
    signal h_blank : STD_LOGIC := '1';
    signal v_blank, v_blank_del : STD_LOGIC := '0';
    signal v_sync_sig, v_sync_sig_del : STD_LOGIC := '1';
    signal r_out, g_out, b_out : STD_LOGIC_VECTOR(7 downto 0) := x"FF";
    
begin

    h_cntr : gen_mod_ctr 
        generic map(
            max => 800)
        port map(
            clk => clk,
            reset => reset_n,
            cnt => column_out,
            roll => v_clk);
    
    v_cntr : gen_mod_ctr
        generic map(
            max => 525)
        port map(
            clk => v_clk,
            reset => reset_n,
            cnt => row_out);
            
     scarFace : scopeFace
        port map(
            row => row_out,
            column => column_out,
            trigger_volt => trigger_volt,
            trigger_time => trigger_time,
            ch1 => ch1,
            ch2 => ch2,
            ch3 => ch3,
            ch4 => ch4,
            r => r_out,
            g => g_out,
            b => b_out);
            
    process(clk)
    begin
        if (rising_edge(clk)) then
            column <= column_out;
            row <= row_out;
            v_blank <= v_blank_del;
            v_sync_sig <= v_sync_sig_del;
            r <= r_out;
            g <= g_out;
            b <= b_out;
            if (reset_n = '0') then                             --reset causes blank screen
                h_blank <= '1';
                h_sync <= '1';
                v_sync_sig <= '1';
            elsif (column_out >= 0 and column_out < 640) then   --Active Video
                h_blank <= '0';
                h_sync <= '1';
            elsif (column_out >= 640 and column_out < 656) then --Front Porch
                h_blank <= '1';
                h_sync <= '1';
            elsif (column_out >= 656 and column_out < 752) then --Sync
                h_blank <= '1';
                h_sync <= '0';
            elsif (column_out >= 752 and column_out < 800) then --Back Porch
                h_blank <= '1';
                h_sync <= '1';
            end if;
        end if;
    end process;
    
    process(v_clk)
    begin
        if (rising_edge(v_clk)) then
            if (row_out = 524) then
                v_blank_del <= '0';
                v_sync_sig_del <= '1';
            elsif (row_out >= 0 and row_out < 479) then        --Active Video
                v_blank_del <= '0';
                v_sync_sig_del <= '1';
            elsif (row_out >= 479 and row_out < 489) then   --Front Porch
                v_blank_del <= '1';
                v_sync_sig_del <= '1';
            elsif (row_out >= 489 and row_out < 491) then   --Sync
                v_blank_del <= '1';
                v_sync_sig_del <= '0';
            elsif (row_out >= 491 and row_out < 524) then   --Back Porch
                v_blank_del <= '1';
                v_sync_sig_del <= '1';
            end if;
        end if;
    end process;

    blank <= h_blank or v_blank;
    v_sync <= v_sync_sig;
    
end behavioral;