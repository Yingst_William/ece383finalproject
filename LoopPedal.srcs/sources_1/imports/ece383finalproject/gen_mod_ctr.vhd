--------------------------------------------------------------------
-- Name:	C2C Will Yingst
-- Date:	Jan 29, 2020
-- File:	gen_mod_ctr.vhd
-- Assn:	Lab 1
-- Crs:	    ECE 383
--
-- Purp:	This file generates an unsigned number which counts up to 
--          the specified generic value and then rolls over to zero
--
-- Documentation:	No Help Received
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity gen_mod_ctr is
    Generic ( max : integer);
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           cnt : out unsigned (9 downto 0);
           roll : out STD_LOGIC);
end gen_mod_ctr;

architecture Behavioral of gen_mod_ctr is
    
    signal processCnt : unsigned (9 downto 0) := "0000000000";
    
begin
    process(clk)
    begin
        if (rising_edge(clk)) then
            if (reset = '0') then
                processCnt <= (others => '0');
                roll <= '0';
            elsif (processCnt < max - 1) then
                processCnt <= processCnt + 1;
                roll <= '0';
            elsif (processCnt = max - 1) then
                processCnt <= (others => '0');
                roll <= '1';
            end if;
        end if;
    end process;
    
    cnt <= processCnt;


end Behavioral;