--------------------------------------------------------------------
-- Name:	C2C Will Yingst
-- Date:	Feb 24, 2020
-- File:	Lab2_cu.vhd
-- Assn:	Lab 2
-- Crs:	    ECE 383
--
-- Purp:	Control Unit/FSM for Lab2 (full functionality)
--
-- Documentation:	No Help Received
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity oscope_fsm is
    Port ( clk     : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           rdy     : in  std_logic;
           sw      : in std_logic_vector(1 downto 0);
           cw      : out std_logic_vector (2 downto 0) );
end oscope_fsm;

architecture Behavioral of oscope_fsm is

    type state_type is (WaitT, WaitR, Write, Inc);
    signal state : state_type := WaitT;

begin

    --------- sw table ----------
    -- (1) address max comparator
    -- (0) trigger AND

    state_process : process(clk)
    begin
        if (rising_edge(clk)) then 
            if (reset_n = '0') then 
                state <= WaitT;
            else
                case state is 
                    when WaitT =>
                        if (sw(0) = '1') then state <= Write; end if;
                    when Write =>
                        state <= Inc;
                    when Inc =>
                        if (sw(1) = '0') then state <= WaitR;
                        else state <= WaitT; end if;
                    when WaitR =>
                        if (rdy = '1') then state <= Write; end if;
                end case;
            end if;
        end if;
    end process;
    
    -- ctr <= cw(1 downto 0)
    -- ctr table ----
    -- reset | "00" | 
    --  hold | "01" |
    --    up | "10" |
    --  load | "11" |
    -----------------
    
    -- WREN <= cw(2)
    
    cw <= "011" when (state = WaitT) else -- ctr load, WREN low
          "001" when (state = WaitR) else -- ctr hold, WREN low
          "101" when (state = Write) else -- ctr hold, WREN hi
          "010"; -- when (state = Inc)    -- ctr up,   WREN low

end Behavioral;
