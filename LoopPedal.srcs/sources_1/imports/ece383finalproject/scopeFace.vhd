--------------------------------------------------------------------
-- Name:	C2C Will Yingst
-- Date:	Jan 29, 2020
-- File:	scopeFace.vhd
-- Assn:	Lab 1
-- Crs:	    ECE 383
--
-- Purp:	This file combinatorially generates pixel-specific RGB 
--          values when fed a row/column signal IAW Lab 1 reqs
--
-- Documentation:	No Help Received
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity scopeFace is
    Port( row : in  unsigned(9 downto 0);
          column : in  unsigned(9 downto 0);
		  trigger_volt: in unsigned (9 downto 0);
		  trigger_time: in unsigned (9 downto 0);
          r : out  std_logic_vector(7 downto 0);
          g : out  std_logic_vector(7 downto 0);
          b : out  std_logic_vector(7 downto 0);
		  ch1: in std_logic;
		  ch2: in std_logic;
		  ch3: in std_logic;
		  ch4: in std_logic);
end scopeFace;

architecture CSAs of scopeFace is

begin

    r <= x"00" when (column >= 640 and column < 800) or (row >= 480 and row < 525) else     --blank signal
    
        --top, bottom, left, right of screen blank
        x"00" when (row < 20) or (row > 420) or (column < 20) or (column > 620) else
        
        --ch 1 trace
        x"FF" when (ch1 = '1') else
        
        --ch 4 trace
        x"FF" when (ch4 = '1') else
        
        --vertical lines
        x"FF" when (column mod 60 = 20) else
                   
        --horizontal lines
        x"FF" when (row mod 50 = 20) else
                   
        --horizontal hashes
        x"FF" when (row < 223) and (row > 217) and (column mod 15 = 5) else 
        
        --vertical hashes
        x"FF" when (column < 323) and (column > 317) and (row mod 10 = 0) else 
        
        --voltage triangle
        x"FF" when ((row = trigger_volt) and (column > 19 and column < 24)) or 
                   ((row = trigger_volt + 1 or row = trigger_volt - 1) and (column > 19 and column < 23)) or 
                   ((row = trigger_volt + 2 or row = trigger_volt - 2) and (column > 19 and column < 22)) else
                   
        --timing triangle
        x"FF" when ((column = trigger_time) and (row > 19 and row < 24)) or 
                   ((column = trigger_time + 1 or column = trigger_time - 1) and (row > 19 and row < 23)) or 
                   ((column = trigger_time + 2 or column = trigger_time - 2) and (row > 19 and row < 22)) else 
                   
        x"00";
        
        
    g <= x"00" when (column >= 640 and column < 800) or (row >= 480 and row < 525) else
    
        --top, bottom, left, right of screen blank
        x"00" when (row < 20) or (row > 420) or (column < 20) or (column > 620) else
        
        --ch 2 trace
        x"FF" when (ch2 = '1') else
        
        --vertical lines
        x"FF" when (column mod 60 = 20) else
                   
        --horizontal lines
        x"FF" when (row mod 50 = 20) else
                   
        --horizontal hashes
        x"FF" when ((row < 223) and (row > 217) and (column mod 15 = 5)) else 
        
        --vertical hashes
        x"FF" when (column < 323) and (column > 317) and (row mod 10 = 0) else 
        
        --voltage triangle
        x"FF" when ((row = trigger_volt) and (column > 19 and column < 24)) or 
                   ((row = trigger_volt + 1 or row = trigger_volt - 1) and (column > 19 and column < 23)) or 
                   ((row = trigger_volt + 2 or row = trigger_volt - 2) and (column > 19 and column < 22)) else      
                   
        --timing triangle
        x"FF" when ((column = trigger_time) and (row > 19 and row < 24)) or 
                   ((column = trigger_time + 1 or column = trigger_time - 1) and (row > 19 and row < 23)) or 
                   ((column = trigger_time + 2 or column = trigger_time - 2) and (row > 19 and row < 22)) else                     
                   
        x"00";
        
        
    b <= x"00" when (column >= 640 and column < 800) or (row >= 480 and row < 525) else 
    
        --top, bottom, left, right of screen blank
        x"00" when (row < 20) or (row > 420) or (column < 20) or (column > 620) else
        
        --ch3 trace
        x"FF" when (ch3 = '1') else
        
        --ch4 trace
        x"FF" when (ch4 = '1') else
        
        --vertical lines
        x"FF" when (column mod 60 = 20) else
                   
        --horizontal lines
        x"FF" when (row mod 50 = 20) else
                   
        --horizontal hashes
        x"FF" when ((row < 223) and (row > 217) and (column mod 15 = 5)) else 
        
        --vertical hashes
        x"FF" when (column < 323) and (column > 317) and (row mod 10 = 0) else 
                   
        x"00";
        
end CSAs;