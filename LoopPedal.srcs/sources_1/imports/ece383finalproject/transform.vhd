--------------------------------------------------------------------
-- Name:	C2C Will Yingst
-- Date:	Feb 24, 2020
-- File:	transform.vhd
-- Assn:	Lab 2
-- Crs:	    ECE 383
--
-- Purp:	transforms 18-bit audio codec output into flipped, 
--          shifted, and scaled 10-bit video.vhd input 
--          (linear transform)
--
-- Documentation:	No Help Received
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
------------------------------------------------------------------------- 
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity transform is
    Port ( sign : in STD_LOGIC_VECTOR (17 downto 0);
           un_sign : out STD_LOGIC_VECTOR (9 downto 0));
end transform;

architecture Behavioral of transform is
    signal in_int : integer;
    signal out_int : integer;
begin
    in_int <= to_integer(unsigned(sign + 131072));
    out_int <= ((in_int / (-100)) + 1530);
    un_sign <= std_logic_vector(to_unsigned(out_int, 10));
end Behavioral;